import RPi.GPIO as GPIO
import socket
import sys

GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)
GPIO.setup(16, GPIO.OUT)

GPIO.setup(7, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)
 
HOST = ''   # Symbolic name meaning all available interfaces
PORT = 8888 # Arbitrary non-privileged port
 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created'
 
try:
    s.bind((HOST, PORT))
except socket.error , msg:
    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()
     
print 'Socket bind complete'
 
s.listen(10)
print 'Socket now listening'
 
#wait to accept a connection - blocking call
conn, addr = s.accept()
 
print 'Connected with ' + addr[0] + ':' + str(addr[1])

while(1):

    #now keep talking with the client
    data = conn.recv(1024)
    print data
    if data == "forward":
        GPIO.output(16, True)
        GPIO.output(12, False)
        GPIO.output(18, True)
    	GPIO.output(7, False)
        
    if data == "backward":
        GPIO.output(12, True)
    	GPIO.output(16, False)
        GPIO.output(7, True)
        GPIO.output(18, False)

    if data == "left":
        GPIO.output(12, False)
    	GPIO.output (16, True)
        
        GPIO.output(7, True)
        GPIO.output (18, False)
        
    if data == "right":
        GPIO.output(12, True)
    	GPIO.output(16, False)
        
        GPIO.output(7, False)
        GPIO.output(18, True)
    
    if data == "shutdown server":
        conn.close()
        s.close()
        break
    else:
        conn.sendall("you gave my life direction")
 
#conn.close()
#s.close()
print "all done"